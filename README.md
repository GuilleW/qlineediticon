# QLineEditIcon

A [QLineEdit](https://doc.qt.io/qt-5/qlineedit.html) with icon. When text is set the icon is replaced by a clear icon button, so user can click and clear it. A shortcut [Escape] is also set to clear the field. QLineEditIcon supports PNG & SVG.

Read [the doc](./html/docs/index.html) for more details about the QLineEditIcon methods.

## Install

Open a shell in your Qt project :

```sh
mkdir vendor
cd vendor
git clone https://gitlab.com/GuilleW/qlineediticon.git

```

## Example


```cpp
#include "vendor/qlineediticon/src/qlineediticon.h"

  QLineEditIcon *input = new QLineEditIcon("icon.svg", "cancel.svg", this);

```

## Contribute
All contributions, code, feedback and strategic advice, are welcome. If you have a question you can contact me directly via email or simply [open an issue](https://gitlab.com/GuilleW/overlayout/issues/new) on the repository. I'm also always happy for people helping me test new features.
