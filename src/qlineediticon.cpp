#include "qlineediticon.h"

QLineEditIcon::QLineEditIcon(QWidget *parent) : QLineEdit(parent)
{
  init();
}

QLineEditIcon::QLineEditIcon(const QString &iconFilename, const QString &cancelFilename, QWidget *parent) : QLineEdit(parent)
{
  init();
  setDefaultIcon(iconFilename);
  setCancelIcon(cancelFilename);
}

void QLineEditIcon::setDefaultIcon(const QString &filename)
{
  loadImage(iconDefault,filename);
}

void QLineEditIcon::setCancelIcon(const QString &filename)
{
  iconCancel->setCursor(Qt::ArrowCursor);
  loadImage(iconCancel,filename);
}

void QLineEditIcon::keyPressEvent(QKeyEvent *event)
{
  QLineEdit::keyPressEvent(event);
  if( event->key() == Qt::Key_Escape ) {
      this->clear();
    }
}

bool QLineEditIcon::eventFilter(QObject *object, QEvent *event)
{

  if (event->type() == QEvent::MouseButtonPress) {
      this->clear();
    }

  // standard event processing
  return QObject::eventFilter(object, event);

}

void QLineEditIcon::init()
{

  int fontHeight = fontMetrics().boundingRect("A").height();
  iconHeight = qCeil(0.8*fontHeight);
  int iconMargin = qCeil(0.4*iconHeight);

  QMargins margin = textMargins();
  margin.setRight(fontHeight+iconMargin);
  setTextMargins(margin);

  iconDefault = new QLabel;
  iconCancel = new QLabel;

  layout = new QHBoxLayout;
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, iconMargin, 0);
  layout->addWidget(iconDefault, 0, Qt::AlignRight);
  iconCancel->hide();
  layout->addWidget(iconCancel, 0, Qt::AlignRight);
  setLayout(layout);

  connect(this, SIGNAL(textChanged(const QString)), this, SLOT(onTextChanged(const QString)));

  // Installs an event filter on this object
  this->iconCancel->installEventFilter(this);

}

void QLineEditIcon::onTextChanged(const QString &text)
{
  if(text.length()){
      this->iconDefault->hide();
      this->iconCancel->show();
    } else{
      this->iconDefault->show();
      this->iconCancel->hide();
    }
}

bool QLineEditIcon::loadImage(QLabel *icon, const QString &filename)
{
  QPixmap pix;
  if(pix.load(filename)){
      pix = pix.scaled(QSize(iconHeight,iconHeight),Qt::KeepAspectRatio, Qt::SmoothTransformation);
      icon->setPixmap(pix);
      return true;
    }
  return false;
}
