#ifndef QLINEEDITICON_H
#define QLINEEDITICON_H

#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMouseEvent>
#include <QtMath>

/**
 * @brief A [QLineEdit](https://doc.qt.io/qt-5/qlineedit.html) with icon.
 * When text is set the icon is replaced by a clear icon button, so user can click and clear it.
 * A shortcut [Escape] is also set to clear the field. QLineEditIcon supports PNG & SVG.
 */
class QLineEditIcon : public QLineEdit
{
  Q_OBJECT

public:

  /**
   * @brief Constructs a QLineEditIcon widget which is a child of parent.
   *
   * You must set setDefaultIcon() and setCancelIcon() later.
   *
   * If parent is 0, the new QLineEditIcon becomes a window. If parent is another widget, this QLineEditIcon becomes a child window inside parent.
   * The new QLineEditIcon is deleted when its parent is deleted.
   *
   * @param parent
   */
  explicit QLineEditIcon(QWidget *parent = nullptr);

  /**
   * @brief Constructs a QLineEditIcon widget which is a child of parent.
   *
   * When text is set the icon is replaced by a clear icon button, so user can click and clear it.
   * A shortcut [Escape] is also set to clear the field. QLineEditIcon supports PNG & SVG.
   *
   * If parent is 0, the new QLineEditIcon becomes a window. If parent is another widget, this QLineEditIcon becomes a child window inside parent.
   * The new QLineEditIcon is deleted when its parent is deleted.
   *
   * @param iconFilename Set icon displayed when no text is set in field.
   * @param cancelFilename Set cancel icon displayed when a text is set in field.
   * @param parent
   */
  explicit QLineEditIcon(const QString &iconFilename, const QString &cancelFilename, QWidget *parent = nullptr);

  /**
   * @brief Set icon displayed when no text is set in field.
   * @param filename
   */
  void setDefaultIcon(const QString &filename);

  /**
   * @brief Set cancel icon displayed when a text is set in field.
   * @param filename
   */
  void setCancelIcon(const QString &filename);

protected:
  /**
   * @brief Used to bind [Escape] key and clear field
   * @param event
   */
  void keyPressEvent(QKeyEvent *event) override;
  /**
   * @brief Used to clear field when cancel icon is clicked
   * @param object
   * @param event
   * @return
   */
  bool eventFilter(QObject *object, QEvent *event) override;

private:
  int iconHeight;
  QLabel *iconDefault = nullptr;
  QLabel *iconCancel = nullptr;
  QHBoxLayout *layout = nullptr;
  void init();
  bool loadImage(QLabel *icon, const QString &filename);

private slots:
  void onTextChanged(const QString &text);

};

#endif // QLINEEDITICON_H
